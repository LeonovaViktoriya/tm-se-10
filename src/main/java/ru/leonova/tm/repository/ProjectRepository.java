package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void persist(@NotNull final Project project) {
        storage.put(project.getProjectId(), project);
    }

    @Override
    public Collection<Project> findAll() {
        return storage.values();
    }

    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String projectId) throws EmptyArgumentException, AccessException {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyArgumentException();
        if (!storage.get(projectId).getUserId().equals(userId)) throw new AccessException();
        return storage.get(projectId);

    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws EmptyArgumentException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Project> projectCollection = new ArrayList<>();
        for (@NotNull final Project project : storage.values()) {
            if (project.getUserId().equals(userId)) {
                projectCollection.add(project);
            }
        }
        return projectCollection;
    }

    @Override
    public void updateProjectName(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) throws EmptyArgumentException, AccessException {
        if (projectId.isEmpty() || name.isEmpty() || userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Project project = storage.get(projectId);
        if(!project.getUserId().equals(userId)) throw new AccessException();
        project.setName(name);
    }

    public boolean isExist(@NotNull final String projectId) throws EmptyArgumentException {
        if (projectId.isEmpty()) throw new EmptyArgumentException();
        return storage.containsKey(projectId);
    }

    @Override
    public void merge(@NotNull final Project project) throws EmptyArgumentException, AccessException {
        if (project.getProjectId() == null || project.getProjectId().isEmpty()) throw new EmptyArgumentException();
        for (@NotNull final Project project1 : storage.values()) {
            if (project1.getProjectId().equals(project.getProjectId())) {
                updateProjectName(project.getUserId(), project1.getProjectId(), project.getName());
            } else {
                persist(project);
            }
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) throws EmptyArgumentException, AccessException {
        if (userId.isEmpty() || project.getProjectId() == null || project.getProjectId().isEmpty()) throw new EmptyArgumentException();
        if (!project.getUserId().equals(userId)) throw new AccessException();
        storage.remove(project.getProjectId());
    }

    @Override
    public void removeAllProjectsByUserId(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Project> projects = findAllByUserId(userId);
        if (projects==null || projects.isEmpty()) throw new EmptyCollectionException();
        projects.clear();
    }

    @Override
    public List<Project> sortedProjectsBySystemDate(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Project> projectCollection = findAll();
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateSystem);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByStartDate(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Project> projectCollection = findAll();
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projectCollection.isEmpty() || projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateStart);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByEndDate(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Project> projectCollection = Objects.requireNonNull(findAllByUserId(userId));
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateEnd);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByStatus(@NotNull final String userId) throws EmptyCollectionException, EmptyArgumentException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Project> projectCollection = Objects.requireNonNull(findAllByUserId(userId));
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projectCollection.isEmpty() || projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getStatus);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws EmptyArgumentException {
        if (projectName.isEmpty() || userId.isEmpty()) throw new EmptyArgumentException();
        for (@NotNull final Project project : storage.values()) {
            if (project.getName().contains(projectName) && project.getUserId().equals(userId)) {
                return project;
            }
        }
        return null;
    }

    @Override
    public Project searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws EmptyArgumentException {
        if (description.isEmpty() || userId.isEmpty()) throw new EmptyArgumentException();
        for (@NotNull final Project project : storage.values()) {
            if (project.getDescription().contains(description) && project.getUserId().equals(userId)) {
                return project;
            }
        }
        return null;
    }

    @Override
    public void load(@NotNull final List<Project> list) {
        list.forEach(project -> storage.put(project.getProjectId(), project));
    }

}

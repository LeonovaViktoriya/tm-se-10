package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ru.leonova.tm.api.repository.ITaskRepository {

//    final private Map<String, Task> storage = new LinkedHashMap<>();

    @Override
    public void persist(@NotNull final Task task) throws EmptyArgumentException {
        if (task.getTaskId().isEmpty()) throw new EmptyArgumentException();
        storage.put(task.getTaskId(), task);
    }

    @Override
    public void merge(@NotNull final Task task) throws EmptyArgumentException {
        @NotNull final String taskId = task.getTaskId();
        if(taskId.isEmpty()) throw new EmptyArgumentException();
        if (isExist(taskId)){
            @NotNull final Task taskUpdate = storage.get(taskId);
            taskUpdate.setName(task.getName());
        }
        else persist(task);
    }

    @Override
    public boolean isExist(@NotNull final String taskId) throws EmptyArgumentException {
        if (taskId.isEmpty()) throw new EmptyArgumentException();
        return storage.containsKey(taskId);
    }

    @Override
    public Collection<Task> findAll(){
        return storage.values();
    }

    @Override
    public Task findOne(@NotNull final String taskId) throws EmptyArgumentException {
        if(taskId.isEmpty()) throw new EmptyArgumentException();
        return storage.get(taskId);
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws EmptyArgumentException {
        if(userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Task> taskCollection = new ArrayList<>();
        for (@NotNull final Task task: storage.values()) {
            if(task.getUserId().equals(userId)){
                taskCollection.add(task);
            }
        }
        return taskCollection;
    }

    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId) throws EmptyArgumentException {
        if(projectId.isEmpty())throw new EmptyArgumentException();
        @NotNull final Collection<Task> taskCollection = new ArrayList<>();
        for (@NotNull final Task task: storage.values()) {
            if(task.getProjectId().equals(projectId)){
                taskCollection.add(task);
            }
        }
        return taskCollection;
    }

    @Override
    public void remove(@NotNull final Task task) throws EmptyArgumentException {
        if(task.getTaskId() == null || task.getTaskId().isEmpty()) throw new EmptyArgumentException();
        storage.remove(task.getTaskId());
    }

    @Override
    public void removeAllTasksCollection(@NotNull final Collection<Task> taskCollection) throws EmptyCollectionException {
        if(taskCollection.isEmpty()) throw new EmptyCollectionException();
        storage.values().removeAll(taskCollection);
    }

    @Override
    public void removeAll() {
        storage.clear();
    }

    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String taskName) throws EmptyArgumentException {
        if(userId.isEmpty() || taskName.isEmpty()) throw new EmptyArgumentException();
        for (@NotNull final Task task: storage.values()) {
            if(task.getName().equals(taskName) && task.getUserId().equals(userId)){
                return task;
            }
        }
        return null;
    }

    @Override
    public List<Task> sortByEndDate(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getDateEnd);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> sortByStartDate(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getDateStart);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> sortBySystemDate(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getDateSystem);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> sortByStatus(@NotNull final String userId) throws EmptyCollectionException, EmptyArgumentException {
        if (userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        if (taskCollection.isEmpty()) throw new EmptyCollectionException();
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getStatus);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public void load(@NotNull final List<Task> list) {
        list.forEach(task -> storage.put(task.getTaskId(), task));
    }

}


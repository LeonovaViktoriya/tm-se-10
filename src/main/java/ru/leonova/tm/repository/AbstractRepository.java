package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.LinkedHashMap;
import java.util.Map;

abstract class AbstractRepository<E> {

    final Map<String, E> storage = new LinkedHashMap<>();

    abstract void persist(@NotNull E e) throws EmptyArgumentException;

    abstract void merge(@NotNull E e) throws EmptyArgumentException, AccessException;
}

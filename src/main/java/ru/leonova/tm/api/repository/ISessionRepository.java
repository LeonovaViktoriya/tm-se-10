package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;

public interface ISessionRepository {

    void persist(@NotNull final Session session) throws EmptyArgumentException;

    void merge(@NotNull final Session session) throws EmptyArgumentException, AccessException;

    void update(String sessionId, RoleType roleType, String userId, String signature, Long timestamp);

    Session findSessionById(String sessionId);

    Session findOne(@NotNull String sessionId) throws EmptyArgumentException;

    Collection<Session> findAll();

    void remove(@NotNull Session session) throws EmptyArgumentException;

    void removeAll();
}

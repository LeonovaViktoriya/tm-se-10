package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    void persist(@NotNull Project project);

    Collection<Project> findAll();

    Project findOneById(@NotNull String userId, @NotNull String projectId) throws EmptyArgumentException, AccessException;

    void merge(@NotNull Project project) throws EmptyArgumentException, AccessException;

    void remove(@NotNull String userId, @NotNull Project project) throws EmptyArgumentException, AccessException;

    void removeAllProjectsByUserId(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    Collection<Project> findAllByUserId(@NotNull String userId) throws EmptyArgumentException;

    void updateProjectName(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws EmptyArgumentException, AccessException;

    boolean isExist(@NotNull String projectId) throws EmptyArgumentException;

    List<Project>  sortedProjectsBySystemDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Project> sortedProjectsByStartDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Project> sortedProjectsByEndDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Project> sortedProjectsByStatus(@NotNull String userId) throws EmptyCollectionException, EmptyArgumentException;

    Project searchProjectByName(@NotNull String userId, @NotNull String projectName) throws EmptyArgumentException;

    Project searchProjectByDescription(@NotNull String userId, @NotNull String description) throws EmptyArgumentException;

    void load(@NotNull List<Project> list) throws EmptyArgumentException;
}

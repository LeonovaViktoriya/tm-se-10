package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {
    void persist(@NotNull Task task) throws EmptyArgumentException;

    void merge(@NotNull Task task) throws EmptyArgumentException;

    Collection<Task> findAll();

    Task findOne(@NotNull String taskId) throws EmptyArgumentException;

    Collection<Task> findAllByUserId(@NotNull String userId) throws EmptyArgumentException;

    Collection<Task> findAllByProjectId(@NotNull String projectId) throws EmptyArgumentException;

    void remove(@NotNull Task t) throws EmptyArgumentException;

    boolean isExist(@NotNull String taskId) throws EmptyArgumentException;

    void removeAllTasksCollection(@NotNull Collection<Task> taskCollection) throws EmptyCollectionException;

    void removeAll();

    Task findOneByName(@NotNull String userId, @NotNull String taskName) throws EmptyArgumentException;

    List<Task> sortByEndDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Task> sortByStartDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Task> sortBySystemDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Task> sortByStatus(@NotNull String userId) throws EmptyCollectionException, EmptyArgumentException;

    void load(@NotNull List<Task> list);
}

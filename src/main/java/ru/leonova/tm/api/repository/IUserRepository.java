package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {
    void persist(@NotNull User user);

    User findOne(@NotNull String userId) throws EmptyArgumentException;

    Collection<User> findAll();

    void remove(@NotNull User user) throws EmptyArgumentException;

    void removeAll();

    void load(@NotNull List<User> list);
}

package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.List;

public interface IProjectService {
    Collection<Project> getCollection();
    @NotNull ProjectRepository getProjectRepository();

    void load(@NotNull List<Project> list) throws EmptyArgumentException;

    void create(@NotNull String userId, @NotNull Project project) throws EmptyArgumentException, AccessException;

    void updateNameProject(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws EmptyArgumentException, AccessException;

    Collection<Project> findAllByUserId(@NotNull String userId) throws EmptyArgumentException;

    void deleteProject(@NotNull String userId, @NotNull String projectId) throws EmptyArgumentException, AccessException;

    void deleteAllProject(String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Project> sortProjectsBySystemDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Project> sortProjectsByStartDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Project> sortProjectsByEndDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Project> sortProjectsByStatus(@NotNull String userId) throws EmptyCollectionException, EmptyArgumentException;

    Project searchProjectByName(@NotNull String userId, @NotNull String projectName) throws EmptyArgumentException;

    Project searchProjectByDescription(@NotNull String userId, @NotNull String description) throws EmptyArgumentException;

    void addAll(List<Project> projects);
}

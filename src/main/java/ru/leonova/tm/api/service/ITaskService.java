package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(@NotNull  String userId, @NotNull final Task task) throws EmptyArgumentException;

    Collection<Task> getCollection();

    void load(@NotNull List<Task> list) throws EmptyArgumentException;

    void updateTaskName(@NotNull String userId, @NotNull String taskId, @NotNull  String taskName) throws EmptyArgumentException;

    void deleteTasksByIdProject(@NotNull String userId, @NotNull String projectId) throws EmptyArgumentException, EmptyCollectionException, AccessException;

    Collection<Task> findAllByUserId(@NotNull String userId) throws EmptyArgumentException;

    Collection<Task> findAllByProjectId(@NotNull String projectId) throws EmptyArgumentException;

    boolean isEmptyTaskList();

    void deleteTask(@NotNull String userId, @NotNull String taskId) throws EmptyArgumentException, AccessException;

    void deleteAllTaskByUserId(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    void deleteAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) throws EmptyArgumentException, EmptyCollectionException, AccessException;

    Task findTaskByName(@NotNull String userId,@NotNull String taskName) throws EmptyArgumentException, AccessException;

    List<Task> sortTasksByEndDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Task> sortTasksByStartDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Task> sortTasksBySystemDate(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    List<Task> sortTasksByStatus(@NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    void addAll(List<Task> tasks);
}

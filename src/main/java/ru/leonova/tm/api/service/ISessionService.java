package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

public interface ISessionService {
    void valid(@NotNull Session session) throws EmptyArgumentException, CloneNotSupportedException, AccessException;

    void closeSession(@NotNull Session session) throws EmptyArgumentException;

    Session openSession(@NotNull String login, @NotNull String password) throws EmptyArgumentException, AccessException;
}

package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;
import java.util.List;

public interface IUserService {
    User getCurrentUser();

    void setCurrentUser(User currentUser);

    void create(@NotNull User user);

    void load(@NotNull List<User> list) throws EmptyArgumentException;

    User authorizationUser(@NotNull String login, @NotNull String password) throws EmptyArgumentException;

    boolean isAuth();

    User getById(@NotNull String userId) throws EmptyArgumentException;

    Collection<User> getCollection();

    void adminRegistration(@NotNull String admin, @NotNull String admin1) throws EmptyArgumentException;

    String md5Apache(@NotNull String password) throws EmptyArgumentException;

    void addAll(List<User> users);
}

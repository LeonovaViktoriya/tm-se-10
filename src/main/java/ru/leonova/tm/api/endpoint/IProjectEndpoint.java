package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    void createProject(@WebParam(name = "userId") @NotNull final String userId, @WebParam(name = "project") @NotNull final Project project) throws EmptyArgumentException, AccessException;

    @WebMethod
    void updateNameProject(@WebParam(name = "userId") @NotNull String userId, @WebParam(name = "projectId") @NotNull String projectId, @WebParam(name = "name") @NotNull String name) throws EmptyArgumentException, AccessException;

    @WebMethod
    Collection<Project> findAllProjectsByUserId(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException;

    @WebMethod
    void deleteProject(@WebParam(name = "userId") @NotNull String userId, @WebParam(name = "projectId") @NotNull String projectId) throws EmptyArgumentException, AccessException;

    @WebMethod
    void deleteAllProject(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    List<Project> sortProjectsBySystemDate(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    List<Project> sortProjectsByStartDate(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    List<Project> sortProjectsByEndDate(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    List<Project> sortProjectsByStatus(@WebParam(name = "userId") @NotNull String userId) throws EmptyCollectionException, EmptyArgumentException;

    @WebMethod
    Project searchProjectByName(@WebParam(name = "userId") @NotNull String userId, @WebParam(name = "projectName") @NotNull String projectName) throws EmptyArgumentException;
}

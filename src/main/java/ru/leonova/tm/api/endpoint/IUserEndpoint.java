package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    User getCurrentUser();

//    @WebMethod
//    void createUser(@WebParam(name = "user") @NotNull User user);

    @WebMethod
    void addUser(@WebParam(name = "l") @NotNull String l, @WebParam(name = "p") @NotNull String p);

//    @WebMethod
//    void loadUserList(@WebParam(name = "list") @NotNull List<User> list) throws EmptyArgumentException;

    @WebMethod
    Collection<User> getCollectionUsers();

    @WebMethod
    User authorizationUser(@WebParam(name = "login") @NotNull String login, @WebParam(name = "password") @NotNull String password) throws EmptyArgumentException;

    @WebMethod
    User getUserById(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException;

    @WebMethod
    void addAllUsers(@WebParam(name = "users") @NotNull List<User> users) throws EmptyCollectionException;
}

package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @WebMethod
    void createTask(@WebParam(name = "userId") @NotNull String userId, @WebParam(name = "task") @NotNull Task task) throws EmptyArgumentException;

    @WebMethod
    Collection<Task> getCollection();

    @WebMethod
    void loadListTask(@WebParam(name = "list") @NotNull List<Task> list) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    void updateTaskName(@WebParam(name = "userId") @NotNull String userId, @WebParam(name = "taskId") @NotNull String taskId, @WebParam(name = "taskName") @NotNull String taskName) throws EmptyArgumentException;

    @WebMethod
    void deleteTasksByIdProject(@WebParam(name = "userId") @NotNull String userId, @WebParam(name = "projectId") @NotNull String projectId) throws EmptyArgumentException, EmptyCollectionException, AccessException;

    @WebMethod
    Collection<Task> findAllTasksByUserId(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException;

    @WebMethod
    Collection<Task> findAllTasksByProjectId(@WebParam(name = "projectId") @NotNull String projectId) throws EmptyArgumentException;

    @WebMethod
    boolean isEmptyTaskList();

    @WebMethod
    void deleteTask(@WebParam(name = "userId") @NotNull String userId, @WebParam(name = "taskId") @NotNull String taskId) throws EmptyArgumentException, AccessException;

    @WebMethod
    void deleteAllTaskByUserId(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    void deleteAllTaskByProjectId(@WebParam(name = "userId") @NotNull String userId, @WebParam(name = "projectId") @NotNull String projectId) throws EmptyArgumentException, EmptyCollectionException, AccessException;

    @WebMethod
    Task findTaskByName(@WebParam(name = "userId") @NotNull String userId, @WebParam(name = "taskName") @NotNull String taskName) throws EmptyArgumentException, AccessException;

    @WebMethod
    List<Task> sortTasksByEndDate(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    List<Task> sortTasksByStartDate(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    List<Task> sortTasksBySystemDate(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    List<Task> sortTasksByStatus(@WebParam(name = "userId") @NotNull String userId) throws EmptyArgumentException, EmptyCollectionException;

    @WebMethod
    void addAllTasks(@WebParam(name = "tasks") @NotNull List<Task> tasks) throws EmptyCollectionException;
}

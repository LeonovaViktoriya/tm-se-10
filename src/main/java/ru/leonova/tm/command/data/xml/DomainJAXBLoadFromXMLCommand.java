package ru.leonova.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;

public class DomainJAXBLoadFromXMLCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jaxb-loadUserList-xml";
    }

    @Override
    public String getDescription() {
        return "Load data to xml with jaxb";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final File file = new File("./data/domain.xml");
        @NotNull final FileReader reader = new FileReader (file);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();

        @NotNull final Domain domainResult = (Domain) unmarshaller.unmarshal(reader);
        serviceLocator.getDomainService().load(domainResult);
        domainResult.getProjects().forEach(project -> System.out.println(project.getName()+" "+project.getUserId()));
        domainResult.getTasks().forEach(task -> System.out.println(task.getName()+" "+task.getUserId()));
        domainResult.getUsers().forEach(user -> System.out.println(user.getRoleType()+" "+user.getUserId()));
    }
}

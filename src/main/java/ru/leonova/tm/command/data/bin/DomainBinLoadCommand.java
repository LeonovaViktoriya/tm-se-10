package ru.leonova.tm.command.data.bin;

import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Domain;

import java.io.*;

public class DomainBinLoadCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "bin-loadUserList";
    }

    @Override
    public String getDescription() {
        return "Load data to binary file";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final File file = new File("./data/data.bin");
        @Cleanup final FileInputStream fileInputStream = new FileInputStream(file);
        @Cleanup final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        System.out.println("Loaded from data.bin");
        System.out.println(domain.getUsers());
        System.out.println(domain.getProjects());
        System.out.println(domain.getTasks());
    }
}

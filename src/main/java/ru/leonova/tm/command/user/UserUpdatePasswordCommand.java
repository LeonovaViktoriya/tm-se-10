package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyArgumentException;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-pass";
    }

    @Override
    public String getDescription() {
        return "Update user password";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws EmptyArgumentException {
        System.out.println("["+getDescription().toUpperCase()+"]");
        @NotNull final User currentUser = serviceLocator.getUserService().getCurrentUser();
        if(currentUser.getPassword().isEmpty())return;
        System.out.println("Enter new password");
        @NotNull String password = getScanner().nextLine();
        password = serviceLocator.getUserService().md5Apache(password);
        currentUser.setPassword(password);
        System.out.println("Password is updated");
    }
}

package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;

public final class UserUpdateRoleCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-role";
    }

    @Override
    public String getDescription() {
        return "Admin can edit role user to admin";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws EmptyArgumentException {
        if (!serviceLocator.getUserService().getCurrentUser().getRoleType().equals(RoleType.ADMIN.getRole())) {
            System.out.println("This option available just for admin");
            return;
        }
        System.out.println("[" + getDescription().toUpperCase() + "]");
        System.out.println("[List user:]");
        @NotNull final Collection<User> userCollection = serviceLocator.getUserService().getCollection();
        int i = 0;
        for (User user : userCollection) {
            i++;
            System.out.println(i+". Login: " + user.getLogin() + ", Password: " + user.getPassword() + ", Id: " + user.getUserId() + ", Role type: " + user.getRoleType());
        }
        System.out.println("Enter id user");
        @NotNull final String userId = getScanner().nextLine();
        @NotNull final User user = serviceLocator.getUserService().getById(userId);
        if (user == null) {
            System.out.println("User with this id not found");
        } else {
            user.setRoleType(RoleType.ADMIN.getRole());
            System.out.println("Role user was update to admin");
        }

    }
}

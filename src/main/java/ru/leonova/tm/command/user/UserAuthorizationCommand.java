package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyArgumentException;

public final class UserAuthorizationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "auth";
    }

    @Override
    public String getDescription() {
        return "Authorization user";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws EmptyArgumentException {
        System.out.println("["+getDescription().toUpperCase()+"]\nEnter user login:");
        @NotNull final String login = getScanner().nextLine();
        System.out.println("Enter user password:");
        @NotNull final String password = getScanner().nextLine();
        @NotNull final User currentUser = serviceLocator.getUserService().authorizationUser(login, password);
        if(currentUser==null){
            System.out.println("This user does not registered");
        }else {
            System.out.println("Authorization was successful");
        }
    }

}

package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;

public final class TaskDeleteCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-t";
    }

    @Override
    public String getDescription() {
        return "Delete task";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() throws AccessException, EmptyArgumentException {
        if (serviceLocator.getTaskService().isEmptyTaskList()) return;
        System.out.println("[DELETE TASK BY ID]\n[Task list:]");
        @NotNull final Collection<Task> taskCollection = serviceLocator.getTaskService().getCollection();
        int i = 0;
        for (Task task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName());
        }
        System.out.println("Enter task id:");
        @NotNull final String taskId = getScanner().nextLine();
        if (taskId == null || taskId.isEmpty()) return;
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        serviceLocator.getTaskService().deleteTask(userId, taskId);
        System.out.println("Task deleted");
    }
}

package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;

public final class TaskUpdateNameCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "up-t";
    }

    @Override
    public String getDescription() {
        return "Update task name";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() throws EmptyArgumentException {
        if (serviceLocator.getTaskService().isEmptyTaskList()) return;
        System.out.println("[UPDATE NAME TASK]\nList tasks:");
        @NotNull final Collection<Task> taskCollection = serviceLocator.getTaskService().getCollection();
        int i = 0;
        for (Task task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName());
        }
        System.out.println("Enter id task for rename:");
        @NotNull final String taskId = getScanner().nextLine();
        if (taskId == null || taskId.isEmpty()) return;
        System.out.println("enter new name for task:");
        @NotNull final String name = getScanner().nextLine();
        if (name == null || name.isEmpty()) return;
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        serviceLocator.getTaskService().updateTaskName(userId, taskId, name);
        System.out.println("Task modified like " + name);
    }
}

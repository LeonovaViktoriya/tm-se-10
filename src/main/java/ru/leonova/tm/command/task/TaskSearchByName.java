package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

public class TaskSearchByName extends AbstractCommand {
    @Override
    public String getName() {
        return "search-t-by-name";
    }

    @Override
    public String getDescription() {
        return "Search task by name";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws EmptyArgumentException, AccessException {
        System.out.println(getDescription());
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        System.out.println("\nEnter name task:");
        @NotNull final String taskName = getScanner().nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findTaskByName(userId, taskName);
        System.out.println(task.getName()+" "+task.getDateStart()+" "+task.getDateEnd());
    }
}

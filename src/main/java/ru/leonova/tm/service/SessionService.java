package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.repository.SessionRepository;
import ru.leonova.tm.utils.SignatureUtil;

import java.util.Objects;

public final class SessionService implements ISessionService {

    private final ServiceLocator serviceLocator;
    private final SessionRepository sessionRepository = new SessionRepository();

    public SessionService(ServiceLocator serviceLocator){
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void valid(@NotNull final Session session) throws EmptyArgumentException, CloneNotSupportedException, AccessException {
        if(session.getSessionId().isEmpty() || session.getUserId().isEmpty() || session.getTimestamp().intValue() == 0 || session.getSignature().isEmpty() || session.getRoleType().getRole().isEmpty())
            throw new EmptyArgumentException();
        @NotNull final Session clone = session.clone();
        @NotNull final String sign = session.getSignature();
        clone.setSignature(null);
        final String signCurrent = SignatureUtil.sign(clone, "signa", 5);
        if (!sign.equals(signCurrent)) throw new AccessException("signatures not equals");
        @NotNull final long TimeStampCurrent = System.currentTimeMillis();
        if (TimeStampCurrent > session.getTimestamp() +(30*60*1000)) throw new AccessException("Session time out");
    }

    @Override
    public void closeSession(@NotNull final Session session) throws EmptyArgumentException {
        sessionRepository.remove(session);
    }

    @Override
    public Session openSession(@NotNull final String login, @NotNull final String password) throws EmptyArgumentException, AccessException {
        if(login.isEmpty() || password.isEmpty()) throw new EmptyArgumentException();
        @NotNull final  User user = serviceLocator.getUserService().authorizationUser(login, password);
        if(user==null) throw new AccessException("This user not found");
        @NotNull final Session session = new Session();
        session.setUserId(user.getUserId());
        session.setRoleType(RoleType.USER);
        final String signature = SignatureUtil.sign(session, "signa", 5);
        if(signature==null) throw new AccessException("Signature is null!");
        session.setSignature(signature);
        session.setTimestamp(System.currentTimeMillis());
        return session;
    }
}

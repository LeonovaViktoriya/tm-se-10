package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    final private IProjectRepository projectRepository;
    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }


    @NotNull
    @Override
    public Collection<Project> getCollection() {
        return projectRepository.findAll();
    }

    @Override
    public @NotNull ProjectRepository getProjectRepository() {
        return null;
    }

    @Override
    public void load(@NotNull final List<Project> list) throws EmptyArgumentException {
        if (list.isEmpty()) throw new EmptyArgumentException();
            projectRepository.load(list);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final Project project) throws EmptyArgumentException, AccessException {
        @NotNull final String projectId = project.getProjectId();
        if(userId.isEmpty() || projectId.isEmpty()) throw new EmptyArgumentException();
        if(!project.getUserId().equals(userId)) throw new AccessException();
        if (isExist(projectId)) projectRepository.merge(project);
        else projectRepository.persist(project);
    }

    private boolean isExist(@NotNull final String projectId) throws EmptyArgumentException {
        if(projectId.isEmpty()) throw new EmptyArgumentException();
        return projectRepository.isExist(projectId);
    }

    @Override
    public void updateNameProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) throws EmptyArgumentException, AccessException {
        if(userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new EmptyArgumentException();
        if (projectRepository.findOneById(userId, projectId) != null && projectRepository.findOneById(userId, projectId).getUserId().equals(userId)) {
            projectRepository.updateProjectName(userId, projectId, name);
        }
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws EmptyArgumentException {
        if(userId.isEmpty()) throw new EmptyArgumentException();
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void deleteProject(@NotNull final String userId, @NotNull final String projectId) throws EmptyArgumentException, AccessException {
        if(projectId.isEmpty() || userId.isEmpty()) throw new EmptyArgumentException();
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if (project != null && project.getUserId().equals(userId)) projectRepository.remove(userId, project);
    }

    @Override
    public void deleteAllProject(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new EmptyArgumentException();
        projectRepository.removeAllProjectsByUserId(userId);
    }

    @Override
    public  List<Project> sortProjectsBySystemDate(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new EmptyArgumentException();
        return projectRepository.sortedProjectsBySystemDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStartDate(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new EmptyArgumentException();
        return projectRepository.sortedProjectsByStartDate(userId);
    }

    @Override
    public List<Project> sortProjectsByEndDate(@NotNull final String userId) throws EmptyArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new EmptyArgumentException();
        return projectRepository.sortedProjectsByEndDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStatus(@NotNull final String userId) throws EmptyCollectionException, EmptyArgumentException {
        if(userId.isEmpty()) throw new EmptyArgumentException();
        return projectRepository.sortedProjectsByStatus(userId);
    }

    @Override
    public Project searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws EmptyArgumentException {
        if(userId.isEmpty() || projectName.isEmpty()) throw new EmptyArgumentException();
        return projectRepository.searchProjectByName(userId, projectName);
    }

    @Override
    public Project searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws EmptyArgumentException {
        if(userId.isEmpty() || description.isEmpty()) throw new EmptyArgumentException();
        return projectRepository.searchProjectByDescription(description, userId);
    }

    @Override
    public void addAll(List<Project> projects) {
        projects.addAll(projectRepository.findAll());
    }
}

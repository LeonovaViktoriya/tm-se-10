package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "domain")
@XmlRootElement
public class Domain implements Serializable{
    @NotNull
    private List<Project> projects = new ArrayList<>();
    @NotNull
    private List<Task> tasks = new ArrayList<>();
    @NotNull
    private List<User> users = new ArrayList<>();
}


package ru.leonova.tm.exeption;

public class EmptyCollectionException extends Exception {

    public EmptyCollectionException(){
        super("List is empty!");
    }
    public EmptyCollectionException(String message){
        super(message);
    }
}
